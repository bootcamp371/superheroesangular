import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HerolistComponent } from './herolist/herolist.component';
import { DetailpageComponent } from './detailpage/detailpage.component';
import { RegisterpageComponent } from './registerpage/registerpage.component';
import { HomeComponent } from './home/home.component';
import { DeleteComponent } from './delete/delete.component';

const appRoutes: Routes = [
  { path: "", component: HomeComponent},
  { path: "Home", component: HomeComponent},
  { path: "FindaHero", component: HerolistComponent },
  { path: "Details", component: DetailpageComponent},
  { path: "Register", component: RegisterpageComponent},
  { path: "Delete", component: DeleteComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HerolistComponent,
    DetailpageComponent,
    RegisterpageComponent,
    HomeComponent,
    DeleteComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
