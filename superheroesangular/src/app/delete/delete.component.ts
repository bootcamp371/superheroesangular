import { Component, OnInit } from '@angular/core';
import { SuperHero } from '../models/super-hero.model';
import { SuperHeroService } from '../providers/super-hero.service';
import { ActivatedRoute } from '@angular/router';
import { HeroPower } from '../models/hero-power.model';
import { HeroPowerService } from '../providers/hero-power.service';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


declare var window: any;
@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit{
  private unsubscriber : Subject<void> = new Subject<void>();
  dob: string = new Date(Date.now()).toDateString();
  displaydob: string = new Date(Date.now()).toDateString();
  wSuperHero: SuperHero = new SuperHero(0,"","","",this.dob);
  wHeroPowers: Array<HeroPower> = [];
  selectedHero: number = 0;
  constructor(private heroService: SuperHeroService,
              private activatedRoute: ActivatedRoute,
              private heroPowerService: HeroPowerService) {}

  ngOnInit() {

    history.pushState(null, '');
    this.activatedRoute.queryParams.subscribe((params) => {
      let id = params['id'];

      this.heroService.getSuperHeroById(id).subscribe(data => {
        this.wSuperHero = data;
        this.dob = data.dob.split("T")[0];
        console.log("This is it: " + this.dob);
        this.displaydob = this.dob.substring(5,10) + "-" + this.dob.substring(0,4);
        
      });

      this.heroPowerService.getHeroPowerById(id).subscribe(data => {
        this.wHeroPowers = data;
        //console.log("This is me" + this.wHeroPowers[2].heroId);
        this.heroService.deleteSuperHero(id).subscribe();

        
      });
    });

    fromEvent(window, 'popstate').pipe(
      takeUntil(this.unsubscriber)
    ).subscribe((_) => {
      history.pushState(null, '');
     // this.showErrorModal(`You can't make changes or go back at this time.`);
    });
    
  }


}
