import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { HeroPower } from '../models/hero-power.model';
import { Powers } from '../models/powers.model';
import { SuperHero } from '../models/super-hero.model';
import { HeroPowerService } from '../providers/hero-power.service';
import { SuperHeroService } from '../providers/super-hero.service';
import { PowersService } from '../providers/powers.service';

declare var window: any;

@Component({
  selector: 'app-detailpage',
  templateUrl: './detailpage.component.html',
  styleUrls: ['./detailpage.component.css']
})
export class DetailpageComponent {
  wUsePower: Array<Powers> = [];
  wHeroPowers: Array<HeroPower> = [];
  wPowers: Array<Powers> = [];
  dob: string = new Date(Date.now()).toDateString();
  displaydob: string = new Date(Date.now()).toDateString();
  detailSuperHero: SuperHero = new SuperHero(0, "", "", "", this.dob);
  wNewPhone: string = "";
  showOrigpic: boolean = false;
  showNewpic: boolean = false;

  formModal: any;
  //newDob: string = this.dob;


  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private heroService: SuperHeroService,
    private heroPowerService: HeroPowerService,
    private powerService: PowersService) { }

  ngOnInit() {

    this.activatedRoute.queryParams.subscribe((params) => {
      let id = params['id'];
      // if (id <= 12) {
      //   this.showOrigpic = true;
      // } else {
      //   this.showNewpic = true;
      // }
      this.heroService.getSuperHeroById(id).subscribe(data => {
        this.detailSuperHero = data;
        this.dob = data.dob.split("T")[0];
        console.log("This is it: " + this.dob);
        //console.log( this.dob[1] + this.dob[2] + this.dob[0]);
        console.log(this.dob);
        this.displaydob = this.dob.substring(5, 10) + "-" + this.dob.substring(0, 4);
        this.wNewPhone = this.detailSuperHero.phone;
      });

      this.heroPowerService.getHeroPowerById(id).subscribe(data => {
        this.wHeroPowers = data;
        //console.log("This is me" + this.wHeroPowers[2].heroId);
      });

      this.powerService.getAllPowers().subscribe(data => {
        this.wPowers = data;
      });
    });
  }

  deleteHero(idInput: string): void {
    this.formModal.hide();
    this.router.navigate(['/Delete'],
      {
        queryParams: {
          id: idInput
        }
      }
    );
  }
  onSubmit() {
    // if (this.checkIn != undefined){
    //   this.newAnimal.checkInDate = this.checkIn;
    // }
    // if (this.checkOut != undefined){
    //   this.newAnimal.checkOutDate = this.checkOut;
    // }

    // this.animalService.updateAnimal(this.newAnimal).subscribe(data => console.dir(data));


  }

  updateHero(detailSuperHero: SuperHero) {
    console.log(detailSuperHero.phone);
    console.log(detailSuperHero.dob);

    console.log("dude, I hit it: " + this.dob);
    this.dob = this.dob.split("T")[0];

    detailSuperHero.phone = this.wNewPhone;
    detailSuperHero.dob = this.dob;
    this.displaydob = this.dob;
    this.displaydob = this.dob.substring(5, 10) + "-" + this.dob.substring(0, 4);
    console.log(detailSuperHero.dob);
    // this.date = this.theDate.toLocaleDateString();
    // this.getReviews.postNewReview(new Reviews(" ", this.userName, this.userComments)).subscribe(data => console.dir(data));
    this.heroService.updateSuperHero(detailSuperHero).subscribe(data => console.dir(data));

    this.formModal.hide();
    // this.powerService.getAllPowers().subscribe(data => {
    //   this.wPowers = data;
    // });
  }

  openModal(detailSuperHero: SuperHero) {
    this.formModal = new window.bootstrap.Modal(
      document.getElementById("updateHero")
    );
    // this.dob = this.detailSuperHero.dob;
    this.formModal.show()
  }

  deleteModal(detailSuperHero: SuperHero) {
    this.formModal = new window.bootstrap.Modal(
      document.getElementById("deleteHero")
    );
    // this.dob = this.detailSuperHero.dob;
    this.formModal.show()
  }


}


