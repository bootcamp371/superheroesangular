import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SuperHero } from '../models/super-hero.model';
import { SuperHeroService } from '../providers/super-hero.service';
import { Powers } from '../models/powers.model';
import { PowersService } from '../providers/powers.service';
import { HeroPowerService } from '../providers/hero-power.service';
import { HeroPower } from '../models/hero-power.model';

@Component({
  selector: 'app-herolist',
  templateUrl: './herolist.component.html',
  styleUrls: ['./herolist.component.css']
})
export class HerolistComponent implements OnInit{

  wSuperHeroes: Array<SuperHero> = [];
  wPowers: Array<Powers> = [];

  //powerObjects: number[] = new Array<number>;
  selectedPower: number = 0;

  constructor(private router: Router, 
              private SuperHeroesService: SuperHeroService,
              private powerService: PowersService,
              private heroPowerService: HeroPowerService) { }

  ngOnInit(): void {
    this.SuperHeroesService.getAllSuperHeroes().subscribe(data => {
      this.wSuperHeroes = data;
      
      this.powerService.getAllPowers().subscribe(data => {
        this.wPowers = data;
      });
    });
  }

  viewDetails(idInput:string):void{
    this.router.navigate(['/Details'],
    {
    queryParams: {
    id: idInput
    }
    }
    );
  }

  onSubmit() {

    console.log(this.selectedPower);
    if (this.selectedPower == 999){
      this.SuperHeroesService.getAllSuperHeroes().subscribe(data => {
        this.wSuperHeroes = data;
      });
    }
    else
    {
    this.heroPowerService.getHerosWithPowerById(this.selectedPower).subscribe(data => {
      this.wSuperHeroes = data;
      console.dir(this.wPowers);
    });
      }
    }
  }
