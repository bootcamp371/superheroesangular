import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SuperHero } from '../models/super-hero.model';
import { SuperHeroService } from '../providers/super-hero.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  wSuperHeroes: Array<SuperHero> = [];



  constructor(private router: Router,
    private SuperHeroesService: SuperHeroService) { }

  ngOnInit(): void {
    this.SuperHeroesService.getAllSuperHeroes().subscribe(data => {
      this.wSuperHeroes = data;


    });
  }

  viewDetails(idInput:string):void{
    this.router.navigate(['/Details'],
    {
    queryParams: {
    id: idInput
    }
    }
    );
  }
}
