export class HeroPower {
    constructor(public id: number,
        public heroId: number,
        public powerId: number,
        public name: string,
        public description: string){
            
        }
}
