export class SuperHero {

    constructor(public id: number,
        public name: string,
        public nickName: string,
        public phone: string,
        public dob: string){
            
        }
}
