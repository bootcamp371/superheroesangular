import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { HeroPower } from '../models/hero-power.model';
import { SuperHero } from '../models/super-hero.model';


@Injectable({
  providedIn: 'root'
})
export class HeroPowerService {

  constructor(private http: HttpClient) { }

  private allHeroPowerEndpoint: string = 'http://localhost:5216/api/HeroPower';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "*"
    })
  };

  getAllHeroPowers() : Observable<HeroPower[]> {
    return this.http.get(this.allHeroPowerEndpoint, this.httpOptions)
    .pipe(map(res => <HeroPower[]>res));
  }

  public getHeroPowerById(heroId: number): Observable<HeroPower[]>{
    return this.http.get(this.allHeroPowerEndpoint + "/" + heroId + "/heroId", this.httpOptions)
    .pipe(map(res => <HeroPower[]>res));
  }

  public getHerosWithPowerById(powerId: number): Observable<SuperHero[]>{
    return this.http.get(this.allHeroPowerEndpoint + "/" + powerId + "/powerId", this.httpOptions)
    .pipe(map(res => <SuperHero[]>res));
  }

  postNewHeroPower(heroPower: HeroPower): Observable<HeroPower> {
   
    return this.http.post<HeroPower>(this.allHeroPowerEndpoint,
      {
        heroid: heroPower.heroId,
        powerId: heroPower.powerId,
        name: heroPower.name,
        description: heroPower.description
      },
      this.httpOptions);
    }
}
