import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Powers } from '../models/powers.model';
import { HeroPower } from '../models/hero-power.model';

@Injectable({
  providedIn: 'root'
})
export class PowersService {

  constructor(private http: HttpClient) { }

  private allPowerEndpoint: string = 'http://localhost:5216/api/Power';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "*"
    })
  };

  getAllPowers() : Observable<Powers[]> {
    return this.http.get(this.allPowerEndpoint, this.httpOptions)
    .pipe(map(res => <Powers[]>res));
  }

  public getPowerById(powerId: number): Observable<Powers[]>{
    return this.http.get(this.allPowerEndpoint + "/" + powerId, this.httpOptions)
    .pipe(map(res => <Powers[]>res));
  }

  postNewPower(power: Powers): Observable<Powers> {
    console.log("Name: " + power.name);
    console.log("Description: " + power.description); 
    return this.http.post<Powers>(this.allPowerEndpoint,
      {
        name: power.name,
        description: power.description
      },
      this.httpOptions);
  }
}
