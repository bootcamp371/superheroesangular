import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { SuperHero } from '../models/super-hero.model';


@Injectable({
  providedIn: 'root'
})
export class SuperHeroService {

  constructor(private http: HttpClient) { }

  private allSuperHeroesEndpoint: string = 'http://localhost:5216/api/SuperHero';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': "*"
    })
  };

  getAllSuperHeroes() : Observable<SuperHero[]> {
    return this.http.get(this.allSuperHeroesEndpoint, this.httpOptions)
    .pipe(map(res => <SuperHero[]>res));
  }

  public getSuperHeroById(heroId: number): Observable<SuperHero>{
    return this.http.get(this.allSuperHeroesEndpoint + "/" + heroId, this.httpOptions)
    .pipe(map(res => <SuperHero>res));
  }

  public addSuperHero(superHero: SuperHero): Observable<SuperHero>{
    return this.http.post(this.allSuperHeroesEndpoint,
      {
        name: superHero.name,
        nickName: superHero.nickName,
        phone: superHero.phone,
        dob: superHero.dob        
      },
      this.httpOptions).pipe(map(res => <SuperHero>res));
  }

  public updateSuperHero(superHero: SuperHero): Observable<SuperHero>{
    return this.http.put(this.allSuperHeroesEndpoint + "/" + superHero.id,
      {
        id: superHero.id,
        name: superHero.name,
        nickName: superHero.nickName,
        phone: superHero.phone,
        dob: superHero.dob        
      },
      this.httpOptions).pipe(map(res => <SuperHero>res));
  }

  public deleteSuperHero(heroId: number): Observable<SuperHero>{
    return this.http.delete(this.allSuperHeroesEndpoint + "/" + heroId, this.httpOptions)
    .pipe(map(res => <SuperHero>res));
  }
}
