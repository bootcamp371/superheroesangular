import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { HeroPower } from '../models/hero-power.model';
import { Powers } from '../models/powers.model';
import { SuperHero } from '../models/super-hero.model';
import { HeroPowerService } from '../providers/hero-power.service';
import { SuperHeroService } from '../providers/super-hero.service';
import { PowersService } from '../providers/powers.service';
import { AstMemoryEfficientTransformer } from '@angular/compiler';


declare var window: any;

@Component({
  selector: 'app-registerpage',
  templateUrl: './registerpage.component.html',
  styleUrls: ['./registerpage.component.css']
})
export class RegisterpageComponent {

  powerObjects: number[] = new Array<number>;
  selectedPowers?: Powers[] = new Array<Powers>();
  newHeroPowers: HeroPower[] = new Array<HeroPower>();
  formModal: any;
  newName: string = "";
  newDescription: string = "";


  wHeroPowers: Array<HeroPower> = [];
  wPowers: Array<Powers> = [];
  dob: string = new Date(Date.now()).toDateString();
  newSuperHero: SuperHero = new SuperHero(0, "", "", "", this.dob);
  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private heroService: SuperHeroService,
    private heroPowerService: HeroPowerService,
    private powerService: PowersService) { }

  ngOnInit() {

    
  this.powerService.getAllPowers().subscribe(data => {
        this.wPowers = data;
      });

  }


  onSubmit() {

    if (this.newSuperHero.name == "" || this.newSuperHero.nickName == "" || this.newSuperHero.phone == ""){
      return;
    }
    this.newSuperHero.dob = this.dob;
    //console.dir(this.powerObjects); 
    this.heroService.addSuperHero(this.newSuperHero).subscribe(data => {
      this.newSuperHero = data;
    
      for (var i = 0; i < this.powerObjects.length; i++) {
        let powers = this.wPowers.find(p => p.id == this.powerObjects[i]);
        
        if (powers != undefined) {
          let newHeroPower = new HeroPower(0, this.newSuperHero.id, powers.id, powers.name, powers.description);
          this.heroPowerService.postNewHeroPower(newHeroPower).subscribe(data => {this.newHeroPowers})
          this.viewDetails(this.newSuperHero.id.toString());
        };
      }

    });
    
  }


  viewDetails(idInput:string):void{
    this.router.navigate(['/Details'],
    {
    queryParams: {
    id: idInput
    }
    }
    );
  }
  addPower() {

    console.log(this.newName);
    console.log(this.newDescription);
    // this.date = this.theDate.toLocaleDateString();
    // this.getReviews.postNewReview(new Reviews(" ", this.userName, this.userComments)).subscribe(data => console.dir(data));
    this.powerService.postNewPower(new Powers(0, this.newName, this.newDescription)).subscribe(data => {
      console.dir(data);
      this.powerService.getAllPowers().subscribe(data => {
          this.wPowers = data;
        });
    });
    this.formModal.hide();
  }
  openModal() {
    this.formModal = new window.bootstrap.Modal(
      document.getElementById("addPower")
    );
    this.formModal.show()
  }
}
